import React, {Component} from 'react';
import {ScrollView, Text, View, TextInput, Alert} from 'react-native';
import {Container, Header, Left} from 'native-base';
import {bindActionCreators} from 'redux';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';

//Import components
//import {Container, Button, Label} from '../../Components';
import style from './style';
//import Icon from 'react-native-vector-icons/FontAwesome';
import * as actions from '../../../action/mealAction';
export class Meal extends Component {
  constructor(props) {
    super(props)
    console.log(this.props);
    //this._onPressButton = this._onPressButton.bind(this);
  }
  press() {
    Actions.pop()
  }
  render(){
    var _that = this;
    return (
      <ScrollView>
        <Container>
          <Header backgroundColor="#00b386">
            <Left>
              <Text>Header</Text>
            </Left>
          </Header>
        </Container>
      </ScrollView>
    )
  }
}


function mapStateToProps(state) {
  return ({
    state:state
  })
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Meal)

// <View style={{
//   flex:1,
//   flexDirection:'column',
//   justifyContent:'center',
//   alignSelf: 'auto'
// }}>
//   <View style={{flex:2, backgroundColor:'skyblue', justifyContent: 'center', alignItems: 'center'}} >
//     <Button onPress={this._onPressButton} title='Hit Me' color='#ddd' />
//   </View>
//   <View style={{flex:2, backgroundColor:'red'}} />
//   <View style={{width: '60%', flex:2,backgroundColor:'green', justifyContent: 'center', alignItems: 'center', margin:'20%'}} >
//     <TextInput onChangeText={(text)=>{
//       console.log(text);
//     }} onSubmitEditing ={(event)=>{
//       console.log('--- ',event.nativeEvent.text);
//     }} placeholder='Type here'
//     style={{width:'100%'}}
//     />
//   </View>
// </View>
