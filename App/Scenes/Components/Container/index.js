import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
export const Container = (props) => {

  return (
    <View style={Style.marginBottom}>
      {props.children}
    </View>
  )
}

const Style = StyleSheet.create({
  'marginBottom':{
    marginBottom: 20
  }
})
