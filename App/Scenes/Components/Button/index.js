import React, {Component} from 'react';
import {TouchableHighlight, Text, StyleSheet} from 'react-native';

export const Button = (props) => {
  function getContent() {
    if(props.children) {
      return props.children;
    }
    return <Text style={props.styles.label}>{props.label}</Text>
  }
  return (
    <TouchableHighlight underlayColor='#ccc'
      onPress = {props.onPress}
      style = {[props.noDefaultStyles ? '' : Style.button,
      props.styles ? props.styles.button : ''
    ]}
      >
      {getContent()}
    </TouchableHighlight>
  )
}

const Style = StyleSheet.create({
  button: {
      alignItems: 'center',
      justifyContent: 'center',
      padding: 20
  },
})
