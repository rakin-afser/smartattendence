import { combineReducers } from 'redux';

import routeReducer from './route-reducer';
import reducer from './app-reducer';

export default combineReducers({
  route: routeReducer,
  reducer: reducer
})
