import * as constants from '../constants';

export function login(userId, password) {
  return {
    type: constants.LOGIN,
    payload: {
      userId: userId,
      password: password
    }
  }
}
