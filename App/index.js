import React from 'react';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider, connect } from 'react-redux';
import { Scene, Actions, Router } from 'react-native-router-flux';

import Home from './Scenes/home';
import Page from './Scenes/page';
import Login from './Scenes/Login';
import Meal from './Scenes/Mess/Meal';

import { persistStore, autoRehydrate } from 'redux-persist';
import { AsyncStorage } from 'react-native'

const navigator = Actions.create(
  <Scene key='root' hideNavBar>
    <Scene key='login' component={Login} initial/>
    <Scene key='home' component={Home}  />
    <Scene key='page' component={Page} />
    <Scene key='mess' component={Meal} >

    </Scene>
  </Scene>
)

const ReduxRouter = connect((state) => ({
  state: state.route
}))(Router);

const reducers = require('./reducers').default;
const store = compose(autoRehydrate())(createStore)(reducers, applyMiddleware());

//test
store.dispatch({
  type: 'data',
  payload: [1,2,3,4]
})
store.subscribe(()=>{
  console.log(' Logger -> Store Updated -> ',store.getState());
})

persistStore(store, {storage: AsyncStorage}, (items) => {
  console.log('restored')
})

export default class App extends React.Component {
  render(){
    return (
      <Provider store={store} >
        <ReduxRouter navigator={navigator} />
      </Provider>
    )
  }
}
