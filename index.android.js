import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Dimensions, Text } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
var geolib = require('geolib');
let { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
export const TimerMixin = require('react-timer-mixin');
const reactMixin = require('react-mixin');
var lat = '',
    lng= '';
    var i = 0;
// import marker from './public/img/map-marker-icon.png';


const mode = 'driving'; // 'walking';
const origin = {latitude: 23.7927404, longitude: 90.4070464};
const destination = {latitude: 23.762673, longitude: 90.3682114};
//const url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${API_KEY}&mode=${mode}`;
const url = `http://maps.googleapis.com/maps/api/directions/json?origin=${origin.latitude},${origin.longitude}&destination=${destination.latitude},${destination.longitude}&sensor=false`;

import App from './App';

export default class EasyLife extends Component {

  constructor() {
    super();
    this.onRegionChange = this.onRegionChange.bind(this)
    this.drawRoute = this.drawRoute.bind(this);
    this.getCurrentLocation = this.getCurrentLocation.bind(this);
    this.geoSuccess = this.geoSuccess.bind(this);
    this.geoError = this.geoError.bind(this);
    // this.geoOptions = this.geoOptions.bind(this)
    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      coords: {latitude: 23.7782183, longitude: 90.3773319}
    };

  }
  onRegionChange(region) {
    console.log('in onRegionChange ->',region);
  }
  drawRoute() {
    console.log('in drawRoute');
    fetch(url)
    .then(response => {
      console.log('response - ', response);
      return response.json();
    })
    .then(responseJson => {
      console.log("responseJson",responseJson);
        if (responseJson.routes.length) {
            this.setState({
                coords: this.decode(responseJson.routes[0].overview_polyline.points) // definition below
            });
        }
    }).catch(e => {console.warn(e)});
  }
  decode(t,e){for(var n,o,u=0,l=0,r=0,d= [],h=0,i=0,a=null,c=Math.pow(10,e||5);u<t.length;){a=null,h=0,i=0;do a=t.charCodeAt(u++)-63,i|=(31&a)<<h,h+=5;while(a>=32);n=1&i?~(i>>1):i>>1,h=i=0;do a=t.charCodeAt(u++)-63,i|=(31&a)<<h,h+=5;while(a>=32);o=1&i?~(i>>1):i>>1,l+=n,r+=o,d.push([l/c,r/c])}return d=d.map(function(t){return{latitude:t[0],longitude:t[1]}})}

  geoSuccess(position) {
    var initialPosition = JSON.stringify(position);
    console.warn(initialPosition);
      var checkInCirle = geolib.isPointInCircle(
        {latitude: position.coords.latitude, longitude: position.coords.longitude},
        {latitude: 23.7927067, longitude: 90.407105},
        50
      );
      checkInCirle == true ? alert('In Cirle') : console.log('Not in Cirle');
      this.setState({
        region : {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA
        }
      })
  }
  geoError(err) {
    console.log('Error -> ', err);
  }
  geoOptions(accuracy, timeout , maximumAge) {
    return {
      enableHighAccuracy: accuracy,
      timeout: timeout,
      // maximumAge: maximumAge
    }
  }
  getCurrentLocation() {
    navigator.geolocation.getCurrentPosition(this.geoSuccess, this.geoError, this.geoOptions(true, 10000, 30000))
    this.watchId = navigator.geolocation.watchPosition(this.geoSuccess, this.geoError, this.geoOptions(false, 10000, 0))
  }
  componentDidMount() {
  this.getCurrentLocation();
  this.drawRoute();
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
  }

  render() {
    return (
      <View>
      <MapView
        provider={ PROVIDER_GOOGLE }
        style={ styles.container }
        initialRegion={this.state.region}
        onRegionChange={this.onRegionChange}
      >
      <MapView.Polyline
        coordinates={[
            origin,
            ...this.state.coords ,
            destination
        ]}
        strokeWidth={4}
      />

      </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  }
});
reactMixin(EasyLife.prototype, TimerMixin);
AppRegistry.registerComponent('EasyLife', () => App);

// <MapView.Marker
//   title = 'ODDUU Ltd.'
//   key = {this.state.region.latitude}
//   coordinate = {{latitude: this.state.region.latitude, longitude: this.state.region.longitude}}
//   />

// <MapView.Polyline
//   coordinates={[
//       origin,
//       ...this.state.coords ,
//       destination
//   ]}
//   strokeWidth={4}
// />
// (position)=> {
//   var initialPosition = JSON.stringify(position);
//   //alert(initialPosition)
//   var checkInCirle = geolib.isPointInCircle(
//     {latitude: position.coords.latitude, longitude: position.coords.longitude},
//     {latitude: 23.7927067, longitude: 90.407105},
//     50
//   );
//   checkInCirle == true ? alert('In Cirle') : console.log('Not in Cirle');
//   console.log('-------');
//   this.setState({
//     region : {
//       latitude: position.coords.latitude,
//       longitude: position.coords.longitude,
//       latitudeDelta: LATITUDE_DELTA,
//       longitudeDelta: LONGITUDE_DELTA
//     }
//   })
// },
// (err) => {
//   console.warn('get getCurrentPosition->'+JSON.stringify(err));
// },
// {
//   enableHighAccuracy: true,
//   timeout: 5000,
//   maximumAge: 3000
// }
